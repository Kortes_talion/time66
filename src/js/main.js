$(document).ready(function() {

	$(".basket-header__link").fancybox({
		padding: 0,
        closeBtn: false,
        fitToView: false
	});

	$(".card__image-link").fancybox();

	$(".card__feature-all").click(function(e){
		e.preventDefault();
		$('.card__description').slideToggle();
	});

	$(".carousel__list").customCarousel({
		nextSelector: ".carousel__arrow_next",
        prevSelector: ".carousel__arrow_prev",
        slideTimeout: 5000
	});

	$('.offer__preview-list').customCarousel({
		nextSelector: ".offer__arrow_next",
        prevSelector: ".offer__arrow_prev",
        disabledClass: "offer__arrow_disabled"
	});

	$('.card__preview-list').customCarousel({
		nextSelector: ".card__preview-arrow_next",
        prevSelector: ".card__preview-arrow_prev",
        disabledClass: "card__preview-arrow_disabled"
	});

	$(".card__preview-link").click(function(e){
		e.preventDefault();
		var self = $(this);
		var parentBlock = self.closest(".card__image");
		var index = self.closest(".card__preview-item").index();
		$(".card__preview-item").removeClass("card__preview-item_active");
		self.closest(".card__preview-item").addClass("card__preview-item_active");
		$(".card__image-link_active").fadeOut(500, function(){
			$(this).removeClass("card__image-link_active");
			parentBlock.find(".card__image-link").eq(index).addClass("card__image-link_active");
		});
	});

	$(".offer__link").click(function(e){
		e.preventDefault();
		var self = $(this);
		var parentBlock = self.closest(".offer__gallery");
		var index = self.closest(".offer__preview-item").index();
		$(".offer__preview-item").removeClass("offer__preview-item_active");
		self.closest(".offer__preview-item").addClass("offer__preview-item_active");
		$(".offer__image_active").fadeOut(500, function(){
			$(this).removeClass("offer__image_active");
			parentBlock.find(".offer__image").eq(index).addClass("offer__image_active");
		});
	});
	
	function carousel(){
		$(".hit__carousel_active .hit__list").customCarousel({
			nextSelector: ".hit__carousel_active .hit__arrow_next",
	        prevSelector: ".hit__carousel_active .hit__arrow_prev",
	        disabledClass: "hit__arrow_disabled"
		});
	}

	carousel();

	$(".hit__link").click(function(e){
		e.preventDefault();
		var self = $(this);
		var index = self.closest(".hit__item").index();
		var parentBlock = self.closest(".hit");
		var curentBlock = parentBlock.find(".hit__carousel").eq(index);
		parentBlock.find(".hit__item").removeClass("hit__item_active");
		self.closest(".hit__item").addClass("hit__item_active");
		parentBlock.find(".hit__carousel").removeClass("hit__carousel_active");
		curentBlock.addClass("hit__carousel_active");
		if (!curentBlock.find(".hit__list").data('options')) {
			carousel();
		}
	});

	$(".viewed__list").customCarousel({
		nextSelector: ".viewed__arrow_next",
        prevSelector: ".viewed__arrow_prev",
        disabledClass: "viewed__arrow_disabled"
	});

	$(".catalog__sort").click(function(e){
		e.preventDefault();
		var self = $(this);
		if (self.hasClass("catalog__sort_true")) {
			self.removeClass("catalog__sort_true");
		} else {
			self.addClass("catalog__sort_true");
		}

	});

	$(".select").customSelect();

	$(".filter__placeholder").click(function(e){
		e.preventDefault();
		var parentBlock = $(this).closest(".filter__select");

		parentBlock.find(".filter__list").slideDown(500);
	});

	$(document).click(function(){
        $(".filter__list").slideUp();
    });

    $(".filter__select").click(function(event){
        event.stopPropagation();
    });

    if ($(".filter__item input").prop('checked')) {
        $(this).closest(".filter__item").addClass("filter__item_active");
    }

    $(".filter__item input").change(function(){
    	var self = $(this);
    	var list = $(".filter__select-list");
    	var index = self.closest(".filter__item").index();
        if (self.prop('checked')) {
            self.closest(".filter__item").addClass("filter__item_active");
            list.find(".filter__select-item").eq(index).addClass("filter__select-item_active");
        } else {
            self.closest(".filter__item").removeClass("filter__item_active");
            list.find(".filter__select-item").eq(index).removeClass("filter__select-item_active");
        }
    });

    $(".filter__select-link").click(function(e){
    	e.preventDefault();
    	var self = $(this);
    	var index = self.closest(".filter__select-item").index();
    	var parentBlock = self.closest(".filter__content");
    	self.closest(".filter__select-item").removeClass("filter__select-item_active");
    	parentBlock.find(".filter__input").eq(index).prop("checked", false);
    	parentBlock.find(".filter__item").eq(index).removeClass("filter__item_active");
    });

	// range
	var range = $(".filter__range");

    range.find('.filter__slider').slider({
        range: true,
        values: [
            range.find(".filter__value_min").val() || range.data("min"),
            range.find(".filter__value_max").val() || range.data("max")
        ],
        min: range.data("min"),
        max: range.data("max"),
        step: range.data("step"),
        slide: function( event, ui ) {
        	var handle = $(".ui-slider-handle");
            range.find(".filter__value_min").val(ui.values[0]).css("left", handle.eq(0).css("left"));
            range.find(".filter__value_max").val(ui.values[1]).css("left", handle.eq(1).css("left"));
        }
    });

    var handle = $(".ui-slider-handle");
    range.find(".filter__value_min").css("left", handle.eq(0).css("left"));
    range.find(".filter__value_max").css("left", handle.eq(1).css("left"));

    range.find(".filter__value_min").change(function() {
        value = Math.max($(this).val(), range.data("min"));
        if ($(this).val() != value) {
            $(this).val() = value;
        };
        range.find('.filter__slider').slider("values", 0 , value);
    });
    range.find(".filter__value_max").change(function() {
        value = Math.max($(this).val(), range.data("max"));
        if ($(this).val() != value) {
            $(this).val() = value;
        };
        range.find('.filter__slider').slider("values", 1 , value);
    });

	if ($("#map").length > 0){
		ymaps.ready(init);
	    var myMap;

	    function init(){     
	        myMap = new ymaps.Map("map", {
	            center: [56.8305,60.5970],
	            zoom: 17,
	            controls: []
	        });

	        myPlacemark = new ymaps.Placemark([56.8305,60.6000], {
	            hintContent: 'Time66'
	        }, {
	         	iconLayout: 'default#image',
	         	iconImageHref: './images/map.png',
	            iconImageSize: [68, 68],
	            iconImageOffset: [-68, -68]
	        });

	        myMap.geoObjects.add(myPlacemark);
	    }
	}
	
});